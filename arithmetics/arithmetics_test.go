package arithmetics

import (
	"fmt"
	"testing"
)

func TestIsPrime(t *testing.T) {
	in := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13}
	expect := []bool{true, true, true, false, true, false, true, false, false, false, true, false, true}
	res := make([]bool, 0, len(in))

	for _, v := range in {
		res = append(res, isPrime(v))
	}

	for i, v := range res {
		if v != expect[i] {
			er := fmt.Sprintf("Expected: %v, got: %v, int: %v", expect[i], v, in[i])
			t.Error(er)
		}
	}
}
