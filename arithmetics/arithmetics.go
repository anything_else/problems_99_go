package arithmetics

import (
	hlp "github.com/michaljaros84/problems_99/helpers"
	ls "github.com/michaljaros84/problems_99/lists"
	"math"
)

//IsPrime - test if int is prime
func IsPrime(test int) bool {
	res := true
	maxDivider := int(math.Sqrt(float64(test)))

	if test < 0 {
		test = -test
	}

	if test == 0 || test == 1 {
		res = false
	} else {
		for i := 2; i <= maxDivider; i++ {
			if test >= 2 && test%i == 0 {
				res = false
			}
		}
	}

	return res
}

//GetPrimeFactors - find all prime factors of int
func GetPrimeFactors(testInteger int) []int {
	res := make([]int, 0, 3)
	max := hlp.SqrtInt(testInteger)

	if testInteger == 0 {
		return res
	}

	for testInteger%2 == 0 {
		res = append(res, 2)
		testInteger = testInteger / 2
	}

	for n := 3; n <= max; n = n + 2 {
		for testInteger%n == 0 {
			res = append(res, n)
			testInteger = testInteger / n
		}
	}

	if testInteger > 1 {
		res = append(res, testInteger)
	}

	return res
}

//PackDupeFactors - get all prime factors and package them in form of [factr, count]
func PackDupeFactors(testInteger int) []interface{} {
	primes := hlp.CastIntToInterfaceSlice(GetPrimeFactors(testInteger))
	return ls.PackAndCountAll(primes)
}

func FilterPrimes(data []int) []int {
	res := make([]int, 0, len(data)/3)

	for _, v := range data {
		if IsPrime(v) {
			res = append(res, v)
		}
	}

	return res
}

//GoldbachComposition - each int can be constructed from 2 primes. Get the primes
func GoldbachComposition(target int) []int {
	res := make([]int, 0, 2)
	goOn := true

	if target == 0 {
		goOn = false
	}

	if x := target - 2; goOn && IsPrime(x) {
		goOn = false
		res = append(res, 2, x)
	}

	for n := 3; ; n = n + 2 {
		if x := target - n; goOn && IsPrime(x) {
			res = append(res, n, x)
			break
		}
	}

	return res
}

//GoldbachCompositionForRange - for int in range (low, up) find even numbers and return their goldbach composition
func GoldbachCompositionForRange(low, up int) [][]int {
	res := make([][]int, 0, (up-low)/2)

	for crnt := low; crnt <= up; crnt++ {
		goOn := true

		if crnt%2 == 0 {
			scnd := crnt - 2

			if IsPrime(scnd) {
				goOn = false
				res = append(res, []int{crnt, 2, scnd})
			}

			for n := 3; ; n = n + 2 {
				if x := crnt - n; goOn && IsPrime(x) {
					res = append(res, []int{crnt, n, x})
					break
				}
			}
		}
	}

	return res
}

//GoldbachCompositionForRangeTreshold - get only when both are more than limit
func GoldbachCompositionForRangeTreshold(low, up, limit int) [][]int {
	res := make([][]int, 0, (up-low)/2)

	if low < 2*limit {
		low = 2 * limit
	}

	for crnt := low; crnt <= up; crnt++ {
		if crnt%2 == 0 {
			for n := FindFirstPrimeAfter(limit); n <= crnt; n = n + 2 {
				if x := crnt - n; x > limit && IsPrime(n) && IsPrime(x) {
					res = append(res, []int{crnt, n, x})
					break
				}
			}
		}
	}

	return res
}

//GreatestCommonDivisor - find largest common divisor of x,y
func GreatestCommonDivisor(x, y int) int {
	for x != y {
		if x > y {
			x = x - y
		}

		if y > x {
			y = y - x
		}
	}

	return x
}

//AreCoprime - see if are coprime
func AreCoprime(x, y int) bool {
	return GreatestCommonDivisor(x, y) == 1
}

//GetAllCoprime - get all coprimes of number
func GetAllCoprime(target int) []int {
	res := make([]int, 0, target/2)
	for i := 1; i < target; i++ {
		work := i
		if AreCoprime(work, target) {
			res = append(res, work)
		}
	}

	return res
}

//EulersTotientNaive - first implementation, naive and 3x slower than main for large ints (try 789542), and will overflow for even larger (7653234567)
func EulersTotientNaive(target int) int {
	return len(GetAllCoprime(target))
}

//EulersTotient - get number of positive int that are coprime to target
func EulersTotient(target int) int {
	factorCount := PackDupeFactors(target)
	parts := make([]int, 0, len(factorCount))
	res := 1

	for _, v := range factorCount {
		switch v.(type) {
		case []int:
			node := v.([]int)
			parts = append(parts, int(EulerTotientAlg(float64(node[0]), float64(node[1]))))
		case []interface{}:
			node := hlp.CastToIntSlice(v.([]interface{}))
			parts = append(parts, int(EulerTotientAlg(float64(node[0]), float64(node[1]))))
		}
	}

	for _, v := range parts {
		res *= v
	}

	return res
}

func EulerTotientAlg(p, m float64) float64 {
	return (p - 1) * math.Pow(p, m-1)
}

//FindFirstPrimeAfter - return first prime >= start
func FindFirstPrimeAfter(start int) int {
	for i := start; ; i++ {
		temp := i
		if IsPrime(temp) {
			return temp
		}
	}
}
