package lists

import "testing"

func TestGetLastElement(t *testing.T) {
	t.Log("Test GetLastElement")
	tmp := GetLastElement([]interface{}{4, 5, 6, 2}).(int)
	if tmp != 2 {
		t.Error("Expected 2 instead got", tmp)
	}
}

func TestGetLastButOne(t *testing.T) {
	t.Log("Test GetLastElementButOne")
	tmp := GetLastElementButOne([]interface{}{"dupa", "jasiu", "karuzela"}).(string)
	if tmp != "jasiu" {
		t.Error("Expected 'jasiu' got", tmp)
	}
}

func TestGetElementAt(t *testing.T) {
	t.Log("Test GetElementAt")
	tmp := GetElementAt([]interface{}{"dupa", "jasiu", "karuzela"}, 0).(string)
	if tmp != "dupa" {
		t.Error("Expected 'dupa' got", tmp)
	}
}

func TestGetElementsNo(t *testing.T) {
	t.Log("Test GetElementsNo")
	tmp := GetElementsNo([]interface{}{"dupa", "jasiu", "karuzela"})
	if tmp != 3 {
		t.Error("Expected 3 got", tmp)
	}
}

func TestReversElements(t *testing.T) {
	t.Log("Test ReversElements")
	tmp := ReversElements([]interface{}{1, 2, 3, 4})
	if !hlp.HaveSameValue(tmp, []interface{}{4, 3, 2, 1}) {
		t.Error("Expected [4 3 2 1] got", tmp)
	}
}

func TestIsPalindrome(t *testing.T) {
	t.Log("Test IsPalindrome")
	test := []interface{}{"dupa", "jasio", "karuzela", "karuzela", "jasio", "dupa"}
	if !IsPalindrome(test) {
		t.Error("Nope:", test)
	}
}

func TestFlattenNestedList(t *testing.T) {
	t.Log("Test FlattenNestedList")
	res := FlattenNestedList([]interface{}{"dupa", "jasio", "karuzela", []interface{}{"karuzela", "jasio", []interface{}{2, 3}, "dupa"}, []interface{}{4, 5}})
	expect := []interface{}{"dupa", "jasio", "karuzela", "karuzela", "jasio", 2, 3, "dupa", 4, 5}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res", res, "Expected", expect)
	}
}

func TestRemoveDuplicates(t *testing.T) {
	t.Log("Test RemoveDuplicates")
	res := RemoveDuplicates([]interface{}{"dupa", "jasio", "karuzela", "karuzela", "jasio", "dupa"})
	expect := []interface{}{"dupa", "jasio", "karuzela"}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res:", res, "Expected:", expect)
	}
}
func TestEliminateConsecutiveDuplicates(t *testing.T) {
	t.Log("Test remove consecutive duplictes")
	res := EliminateConsecutiveDuplicates([]interface{}{"a", "a", "b", "a", "a", "a", "c", "c", "b", "b"})
	expect := []interface{}{"a", "b", "a", "c", "b"}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res:", res, "Expected:", expect)
	}
}

func TestMultiply(t *testing.T) {
	t.Log("Test multiply")
	res := Multiply([]interface{}{"a", "b", "c"}, 3)
	expect := []interface{}{"a", "a", "a", "b", "b", "b", "c", "c", "c"}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res:", res, "Expected:", expect)
	}
}

func TestDropN(t *testing.T) {
	t.Log("Test drop n")
	res := DropN([]interface{}{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, 2)
	expect := []interface{}{1, 3, 5, 7, 9, 11, 13, 15}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res:", res, "Expected:", expect)
	}
}

func TestExtractSlice(t *testing.T) {
	t.Log("Test Extract Slice")
	res := ExtractSlice([]interface{}{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 3, 6)
	expect := []interface{}{3, 4, 5, 6, 7, 8, 9}
	if !hlp.HaveSameValue(res, expect) {
		t.Error("Nope. Res:", res, "Expected:", expect)
	}
}
