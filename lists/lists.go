package lists

import (
	hlp "github.com/michaljaros84/problems_99/helpers"
	rfl "reflect"
)

//GetLastElement - find last element in collection
func GetLastElement(ar []interface{}) interface{} {
	l := len(ar) - 1
	return ar[l]
}

//GetLastElementButOne - find next to last element of collection
func GetLastElementButOne(ar []interface{}) interface{} {
	l := len(ar) - 2
	return ar[l]
}

//GetElementAt - find element at index
func GetElementAt(ar []interface{}, i int) interface{} {
	return ar[i]
}

//GetElementsNo - get no of elements
func GetElementsNo(ar []interface{}) int {
	return len(ar)
}

//ReversElements - reverse elements
func ReversElements(ar []interface{}) []interface{} {
	ln := len(ar) - 1
	ret := make([]interface{}, len(ar), cap(ar))

	for i, v := range ar {
		ret[ln-i] = v
	}

	return ret
}

//IsPalindrome - see if collection is palindrome
func IsPalindrome(ar []interface{}) bool {
	top := len(ar) - 1
	for _, v := range ar {
		if v != ar[top] {
			return false
		}

		top--
	}

	return true
}

//FlattenNestedList - get inner elements of collection to top
func FlattenNestedList(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, 0)
	for _, v := range ar {
		switch rfl.TypeOf(v).Kind() {
		case rfl.Slice:
			for _, val := range FlattenNestedList(v.([]interface{})) {
				res = append(res, val)
			}
		default:
			res = append(res, v)
		}
	}

	return res
}

//RemoveDuplicates - remove duplicates from collection
func RemoveDuplicates(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, 0)

	for _, v := range ar {
		if !hlp.Contains(res, v) {
			res = append(res, v)
		}
	}
	return res
}

//EliminateConsecutiveDuplicates - remove consecutive duplicates
func EliminateConsecutiveDuplicates(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, len(ar)/2)
	for i, v := range ar {
		if i == 0 || v != ar[i-1] {
			res = append(res, v)
		}
	}

	return res
}

//PackDups - package duplicates into inner array
func PackDups(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, len(ar)/2)

	for i, v := range ar {

		if i == 0 || v != ar[i-1] {
			pack := make([]interface{}, 0)
			res = append(res, append(pack, v))
		} else {
			mainIndx := len(res) - 1
			innerSlice := res[mainIndx]

			switch rfl.TypeOf(innerSlice).Kind() {
			case rfl.Slice:
				tmp := innerSlice.([]interface{})
				if tmp[0] == v {
					tmp = append(tmp, v)
					res[mainIndx] = tmp
				}
			}

		}
	}

	return res
}

//PackAndCount - package duplicates and attach count
func PackAndCount(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, len(ar))
	packed := PackDups(ar)

	for _, v := range packed {
		switch rfl.TypeOf(v).Kind() {
		case rfl.Slice:
			tmpValue := v.([]interface{})

			if len(tmpValue) > 1 {
				node := make([]interface{}, 0, 2)
				node = append(node, len(tmpValue))
				node = append(node, tmpValue[0])
				res = append(res, node)
			} else {
				res = append(res, tmpValue[0])
			}
		}
	}

	return res
}

//PackAndCountAll - package all parts and add count een if only 1
func PackAndCountAll(ar []interface{}) []interface{} {
	packed := PackAndCountDirect(ar)
	res := make([]interface{}, 0, len(ar))

	for _, v := range packed {
		switch rfl.TypeOf(v).Kind() {
		case rfl.Int:
			res = append(res, []int{v.(int), 1})
		default:
			res = append(res, v)
		}
	}

	return res
}

//PackAndCountDirect - same as above direct implementation
func PackAndCountDirect(ar []interface{}) []interface{} {
	cnt := 0
	res := make([]interface{}, 0, len(ar)/2)
	var currVal interface{}

	branchOnCnt := func() {
		if cnt > 1 {
			node := make([]interface{}, 0, 2)
			node = append(node, currVal, cnt)
			res = append(res, node)
		} else if cnt == 1 {
			res = append(res, currVal)
		}
	}

	for i, v := range ar {
		if currVal != v {
			branchOnCnt()
			cnt = 1
			currVal = v

		} else {
			cnt++
		}

		if len(ar)-1 == i {
			branchOnCnt()
		}
	}

	return res
}

//Unpack - revert PackAndCountDirect product
func Unpack(ar []interface{}) []interface{} {
	res := make([]interface{}, 0, len(ar)*2)

	for _, v := range ar {
		switch rfl.TypeOf(v).Kind() {
		case rfl.Slice:
			node := v.([]interface{})
			if len(node) != 2 {
				return nil
			}

			cnt := node[0].(int)
			elem := node[1].(interface{})

			for x := 0; x < cnt; x++ {
				res = append(res, elem)
			}
		default:
			res = append(res, v)
		}
	}

	return res
}

//Multiply - multiply each element count
func Multiply(ar []interface{}, mltplr int) []interface{} {
	res := make([]interface{}, 0, len(ar)*2)

	for _, v := range ar {
		for x := 0; x < mltplr; x++ {
			res = append(res, v)
		}
	}

	return res
}

//DropN - remove each n-th element of collection
func DropN(ar []interface{}, n int) []interface{} {
	res := make([]interface{}, 0, len(ar))
	nextdrop := n - 1

	for i, v := range ar {
		if nextdrop != i {
			res = append(res, v)
		} else {
			nextdrop = i + n
		}
	}

	return res
}

//SplitInTwo - split collection in two, for length of first product
func SplitInTwo(ar []interface{}, frstLng int) ([]interface{}, []interface{}) {
	res1 := ar[:frstLng]
	res2 := ar[frstLng:]

	return res1, res2
}

//ExtractSlice - extract slice for start and end index
func ExtractSlice(ar []interface{}, start, end int) []interface{} {
	res := ar[start : end+1]
	return res
}

//RotateNLeft - move element n to left
func RotateNLeft(ar []interface{}, n int) []interface{} {
	res := make([]interface{}, 0, len(ar))
	res = append(res, ar[n:]...)
	res = append(res, ar[:n]...)

	return res
}

//RemoveN - remove element at n
func RemoveN(ar []interface{}, n int) []interface{} {
	res := make([]interface{}, 0, len(ar)-1)
	res = append(res, ar[:n-1]...)
	res = append(res, ar[n:]...)

	return res
}

//InsertAt - insert element into array at index
func InsertAt(ar []interface{}, at int, val interface{}) []interface{} {
	res := make([]interface{}, 0, len(ar)+1)

	if at < len(ar) {
		res = append(res, ar[:at]...)
		res = append(res, val)
		res = append(res, ar[at:]...)
	}

	return res
}

//CreateListInRange - create a list of int from min to max inclusive
func CreateListInRange(min, max int) []int {
	res := make([]int, 0, max-min+1)
	for ; min <= max; min++ {
		res = append(res, min)
	}

	return res
}

//RandomNoFrom - get n random elements from collection
func RandomNoFrom(ar []interface{}, n int) []interface{} {
	res := make([]interface{}, 0, n)
	used := make([]int, 0, n)

	for x := 0; x < n; x++ {
		i := hlp.GetUniqueindex(used, len(ar))
		used = append(used, i)
		res = append(res, ar[i])
	}

	return res
}

//Lotto - get random n elements from collection
func Lotto(n, top int) []int {
	res := make([]int, 0, n)

	for x := 0; x < n; x++ {
		v := hlp.RandomNonZero(top + 1)
		if hlp.Contains(hlp.CastIntToInterfaceSlice(res), v) {
			x--
		} else {
			res = append(res, v)
		}
	}

	return res
}

//Permutation - permute collection at random
func Permutation(ar []interface{}) []interface{} {
	elCount := len(ar)
	res := make([]interface{}, 0, elCount)
	used := make([]int, 0, elCount)

	for i := 0; i < elCount; i++ {
		index := hlp.GetUniqueindex(used, elCount)
		used = append(used, index)
		res = append(res, ar[index])
	}

	return res
}

//GenerateAllCombinations - generate the combinations of K distinct objects chosen from the N elements of a list
func GenerateAllCombinations(ar []interface{}, n int) {

}
