package helpers

import (
	"math"
	"math/rand"
)

//RandomNonZero - generate random non zero int
func RandomNonZero(seed int) int {
	res := rand.Intn(seed)

	if res == 0 {
		res = RandomNonZero(seed)
	}

	return res
}

//GetUniqueindex - generate random integer not conained in slice (used)
func GetUniqueindex(used []int, seed int) int {
	index := rand.Intn(seed)
	if Contains(CastIntToInterfaceSlice(used), index) {
		return GetUniqueindex(used, seed)
	}

	return index
}

//CastIntToInterfaceSlice - cast interger slice (ar) to []interface{}
func CastIntToInterfaceSlice(ar []int) []interface{} {
	ret := make([]interface{}, len(ar), cap(ar))
	for i, v := range ar {
		ret[i] = v
	}
	return ret
}

//Contains - check if slice (ar) Contains element (el)
func Contains(ar []interface{}, el interface{}) bool {
	has := false
	for _, v := range ar {
		if v == el {
			has = true
			break
		}
	}

	return has
}

//CastToIntSlice - cast slice (ar) to int slice
func CastToIntSlice(ar []interface{}) []int {
	ret := make([]int, len(ar), cap(ar))
	for i, v := range ar {
		ret[i] = v.(int)
	}
	return ret
}

//HaveSameValue - compare if two slices have same value
func HaveSameValue(ar, ar2 []interface{}) bool {
	for i := range ar {
		if ar[i] != ar2[i] {
			return false
		}
	}

	return true
}

//SqrtInt - get square root of int as int
func SqrtInt(testInteger int) int {
	return int(math.Sqrt(float64(testInteger)))
}
